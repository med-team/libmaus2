#/*
    libmaus2
    Copyright (C) 2009-2013 German Tischler
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_LZ_BGZFDEFLATEOUTPUTCALLBACKBAMNUMERICALINDEX_HPP)
#define LIBMAUS2_LZ_BGZFDEFLATEOUTPUTCALLBACKBAMNUMERICALINDEX_HPP

#include <libmaus2/lz/BgzfDeflateOutputCallback.hpp>
#include <libmaus2/bambam/BamNumericalIndexGenerator.hpp>
#include <sstream>
#include <iomanip>

namespace libmaus2
{
	namespace bambam
	{
		struct BgzfDeflateOutputCallbackBamNumericalIndex : public ::libmaus2::lz::BgzfDeflateOutputCallback
		{
			typedef BgzfDeflateOutputCallbackBamNumericalIndex this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;
			typedef std::shared_ptr<this_type> shared_ptr_type;

			::libmaus2::bambam::BamNumericalIndexGenerator generator;

			BgzfDeflateOutputCallbackBamNumericalIndex(
				std::string const & fn,
				uint64_t const rmod,
				unsigned int const rverbose = 0,
				bool const rvalidate = true, bool const rdebug = false
			) : generator(fn,rmod,rverbose,rvalidate,rdebug)
			{

			}

			void operator()(
				uint8_t const * in,
				uint64_t const incnt,
				uint8_t const * /* out */,
				uint64_t const outcnt
			)
			{
				generator.addBlock(in /* uncomp data */,outcnt /* comp size */,incnt /* uncomp size */);
			}

			void flush()
			{
				generator.flush();
			}
		};
	}
}
#endif
