/*
    libmaus2
    Copyright (C) 2009-2013 German Tischler
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_LZ_BGZFHEADERFUNCTIONS_HPP)
#define LIBMAUS2_LZ_BGZFHEADERFUNCTIONS_HPP

#include <libmaus2/types/types.hpp>
#include <libmaus2/lz/BgzfConstants.hpp>
#include <libmaus2/lz/GzipHeader.hpp>
#include <libmaus2/lz/ZlibInterface.hpp>
#include <libmaus2/exception/LibMausException.hpp>
#include <cstring>
#include <zlib.h>

namespace libmaus2
{
	namespace lz
	{
		struct BgzfDeflateHeaderFunctions : public BgzfConstants
		{
			private:
			// maximum space used for compressed version of a block of size blocksize
			static uint64_t getFlushBound(unsigned int const blocksize, int const level);
			static uint64_t getReqBufSpace(int const level);

			public:
			static void deflateinitz(libmaus2::lz::ZlibInterface * p, int const level);
			static void deflatedestroyz(libmaus2::lz::ZlibInterface * p);
			static uint64_t getReqBufSpaceTwo(int const level);
			static void setupHeader(uint8_t * const outbuf);

			static uint8_t const * fillHeaderFooter(
				#if !defined(LIBMAUS2_HAVE_LIBDEFLATE)
				libmaus2::lz::ZlibInterface * zintf,
				#endif
				uint8_t const * const pa,
				uint8_t * const outbuf,
				unsigned int const payloadsize,
				unsigned int const uncompsize
			);

			static uint64_t getOutBufSizeTwo(int const level);
		};
	}
}
#endif
