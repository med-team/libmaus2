/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_GTF_GENCHRCOMPARATOR_HPP)
#define LIBMAUS2_GTF_GENCHRCOMPARATOR_HPP

#include <cstring>

namespace libmaus2
{
	namespace gtf
	{
		struct GeneChrComparator
		{
			char const * id;

			GeneChrComparator(char const * rid)
			: id(rid)
			{
			}

			char const * get(libmaus2::gtf::Gene const & G) const
			{
				return id + G.chr_offset;
			}

			bool operator()(libmaus2::gtf::Gene const & A, libmaus2::gtf::Gene const & B) const
			{
				return strcmp(get(A),get(B)) < 0;
			}

		};
	}
}
#endif
