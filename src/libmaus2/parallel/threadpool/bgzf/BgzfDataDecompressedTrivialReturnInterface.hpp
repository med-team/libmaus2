/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFDATADECOMPRESSEDTRIVIALRETURNINTERFACE_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFDATADECOMPRESSEDTRIVIALRETURNINTERFACE_HPP

#include <libmaus2/parallel/threadpool/bgzf/ThreadPoolBGZFDecompressControl.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bgzf
			{
				struct BgzfDataDecompressedTrivialReturnInterface : public BgzfDataDecompressedInterface
				{
					virtual void dataDecompressed(
						// libmaus2::parallel::threadpool::input::ThreadPoolInputInfo * inputinfo,
						libmaus2::util::shared_ptr<BgzfBlockInfo> blockinfo,
						uint64_t const blockid,
						uint64_t const subid,
						uint64_t const absid,
						libmaus2::util::shared_ptr<GenericBlock> ddata,
						uint64_t const n
					)
					{
						uint64_t const streamid = ddata->streamid.load();

						auto decompressHandler = ddata->decompressHandler.load();
						BgzfBlockInfoEnqueDecompressHandler::OutputQueueInfo OQI = decompressHandler->getOutputQueue(streamid);

						OQI.push(libmaus2::util::shared_ptr<BgzfDecompressedBlock>(new BgzfDecompressedBlock(streamid,blockid,subid,absid,ddata,n,blockinfo)));

						libmaus2::util::shared_ptr<BgzfDecompressedBlock> block;
						while ( OQI.pop(block) )
						{
							decompressHandler->returnBlock(streamid,block->ddata);
							decompressHandler->TPBRC.BITC.putInfo(streamid,block->blockinfo);
							OQI.next();
						}

						uint64_t const lfinished = ++(decompressHandler->getAbsBlockFinished(streamid));

						if (
							decompressHandler->TPBRC.BITC.parseFinished(streamid)
							&&
							lfinished == decompressHandler->getAbsBlock(streamid).load()
						)
						{
							std::cerr << "stream decomp finished " << streamid << std::endl;

							uint64_t const nfinished = ++decompressHandler->numStreamsDecompressFinished;

							if ( nfinished == decompressHandler->numstreams )
							{
								decompressHandler->TP.terminate();
							}
						}
					}
				};

			}
		}
	}
}
#endif
