/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFDATADECOMPRESSEDINTERFACE_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFDATADECOMPRESSEDINTERFACE_HPP

#include <libmaus2/parallel/threadpool/bgzf/GenericBlock.hpp>
#include <libmaus2/parallel/threadpool/bgzf/BgzfBlockInfo.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bgzf
			{
				struct BgzfDataDecompressedInterface
				{
					virtual ~BgzfDataDecompressedInterface() {}
					virtual void dataDecompressed(
						// libmaus2::parallel::threadpool::input::ThreadPoolInputInfo * inputinfo,
						libmaus2::util::shared_ptr<BgzfBlockInfo> blockinfo,
						uint64_t const blockid,
						uint64_t const subid,
						uint64_t const absid,
						libmaus2::util::shared_ptr<GenericBlock> ddata,
						uint64_t const n
					) = 0;
				};
			}
		}
	}
}
#endif
