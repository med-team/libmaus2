/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFBLOCKINFO_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFBLOCKINFO_HPP

#include <libmaus2/parallel/threadpool/bgzf/BgzfBlockSummary.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bgzf
			{
				/**
				 * info of one BGZF block
				 **/
				struct BgzfBlockInfo
				{
					typedef BgzfBlockInfo this_type;
					typedef std::shared_ptr<this_type> shared_ptr_type;

					libmaus2::util::atomic_shared_ptr<BgzfBlockSummary> owner;
					std::atomic<uint64_t> subid;
					std::atomic<uint64_t> absid;
					std::atomic<char *> from;
					std::atomic<char *> to;
					std::atomic<bool> eof;

					void reset(libmaus2::util::shared_ptr<BgzfBlockSummary> rowner, uint64_t const rsubid, uint64_t const rabsid, char * rfrom, char * rto, bool const reof)
					{
						owner.store(rowner);
						subid.store(rsubid);
						absid.store(rabsid);
						from.store(rfrom);
						to.store(rto);
						eof.store(reof);
					}

					bool operator<(BgzfBlockInfo const & O) const
					{
						assert ( owner.load() );
						assert ( O.owner.load() );

						if ( *(owner.load()) < *(O.owner.load()) )
							return true;
						else if ( *(O.owner.load()) < *(owner.load()) )
							return false;
						else
							return absid < O.absid;
					}
				};
			}
		}
	}
}
#endif
