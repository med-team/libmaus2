/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_INPUT_INPUTTHREADTRIVIALRETURNINTERFACE_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_INPUT_INPUTTHREADTRIVIALRETURNINTERFACE_HPP

#include <libmaus2/parallel/threadpool/input/InputThreadControl.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace input
			{
				template<typename _lock_type>
				struct InputThreadTrivialReturnInterface : public InputThreadCallbackInterface
				{
					typedef _lock_type lock_type;
					libmaus2::parallel::threadpool::ThreadPool & TP;
					InputThreadControl & IC;
					lock_type & lock;
					std::ostream & errstr;

					InputThreadTrivialReturnInterface(
						libmaus2::parallel::threadpool::ThreadPool & rTP,
						InputThreadControl & rIC,
						lock_type & rlock,
						std::ostream & rerrstr
					)
					: TP(rTP), IC(rIC), lock(rlock), errstr(rerrstr)
					{
					}

					virtual void inputThreadCallbackInterfaceBlockRead(ThreadPoolInputInfo * inputinfo)
					{
						std::vector< libmaus2::util::shared_ptr<ThreadPoolInputBlock> > V;
						/* bool const eof = */ inputinfo->getBlocks(V);

						uint64_t batchsize = V.size();

						{
							typename lock_type::scope_lock_type slock(lock);
							errstr << "[V] got batch of size " << batchsize << " for stream " << inputinfo->streamid
								<< " bytes read " << inputinfo->bytesRead.load()
								<< " speed " << inputinfo->getSpeed()/(1024.0*1024.0)
								<< std::endl;
						}

						for ( uint64_t i = 0; i < V.size(); ++i )
							IC.returnBlock(V[i]);

						if ( IC.readFinished() )
							TP.terminate();
					}
				};
			}
		}
	}
}
#endif
