/*
    libmaus2
    Copyright (C) 2021 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BAM_THREADPOOLBAMPARSETRIVIALRETURNHANDLER_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BAM_THREADPOOLBAMPARSETRIVIALRETURNHANDLER_HPP

#include <libmaus2/parallel/threadpool/bam/ThreadPoolBamParseControl.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bam
			{
				struct ThreadPoolBamParseTrivialReturnHandler : public ThreadPoolBamParseHandler
				{
					virtual void handle(ThreadPoolBamParseControl * control, std::size_t const streamid)
					{
						auto & context = control->getContext(streamid);
						libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::bgzf::GenericBlock> block;
						while ( context.getOutputQueueElement(block) )
						{
							auto decompressHandler = block->decompressHandler.load();
							bool const eof = block->eof.load();

							// return decompressed block
							decompressHandler->returnBlock(block->streamid.load(),block);

							context.bumpOutNext();

							if ( eof )
							{
								std::cerr << "found parse eof on stream " << block->streamid.load() << std::endl;

								bool const term = control->notifyEOF(block->streamid.load());

								if ( term )
								{
									std::cerr << "calling terminate" << std::endl;

									control->TP.terminate();
								}
							}
						}
					}
				};
			}
		}
	}
}
#endif
