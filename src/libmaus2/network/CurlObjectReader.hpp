/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_NETWORK_CURLOBJECTREADER_HPP)
#define LIBMAUS2_NETWORK_CURLOBJECTREADER_HPP

#include <libmaus2/network/CurlObjectThreadCallable.hpp>
#include <libmaus2/network/HttpCurlResponseAcceptor.hpp>
#include <libmaus2/network/FtpCurlResponseAcceptor.hpp>

namespace libmaus2
{
	namespace network
	{
		struct CurlObjectReader
		{
			typedef CurlObjectReader this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;
			typedef std::shared_ptr<this_type> shared_ptr_type;

			std::string const url;
			libmaus2::network::CurlResponseAcceptor::unique_ptr_type pacceptor;
			libmaus2::network::CurlObject obj;
			libmaus2::parallel::StdThreadCallable::unique_ptr_type pcall;
			libmaus2::parallel::StdThread thread;
			libmaus2::network::CurlObjectThreadCallable & callable;

			static bool startsWith(std::string const & url, std::string const & prefix);
			static libmaus2::network::CurlResponseAcceptor::unique_ptr_type constructAcceptor(std::string const & url);

			CurlObjectReader(std::string const & url, std::size_t const freelistsize = 4);

			std::size_t read(char * p, std::size_t n);
		};
	}
}
#endif
