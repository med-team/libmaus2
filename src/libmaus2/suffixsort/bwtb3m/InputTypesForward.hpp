/**
    libmaus2
    Copyright (C) 2009-2016 German Tischler
    Copyright (C) 2011-2014 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#if ! defined(LIBMAUS2_SUFFIXSORT_BWTB3M_INPUTTYPESFORWARD_HPP)
#define LIBMAUS2_SUFFIXSORT_BWTB3M_INPUTTYPESFORWARD_HPP

#include <libmaus2/suffixsort/bwtb3m/BwtMergeSortOptions.hpp>

struct InputTypesForward
{
	template<typename input_type>
	static uint64_t getFileSize(std::string const & filename)
	{
		uint64_t const fs = input_type::linear_wrapper::getFileSize(filename);
		return fs;
	}

	template<unsigned int wordsize>
	static uint64_t getFileSize(std::string const & filename, libmaus2::suffixsort::bwtb3m::BwtMergeSortOptions::bwt_merge_input_type const inputtype)
	{
		switch ( inputtype )
		{
			case libmaus2::suffixsort::bwtb3m::BwtMergeSortOptions::bwt_merge_input_type_bytestream:
				return getFileSize<libmaus2::suffixsort::ByteInputTypes<wordsize> >(filename);
			case libmaus2::suffixsort::bwtb3m::BwtMergeSortOptions::bwt_merge_input_type_compactstream:
				return getFileSize<libmaus2::suffixsort::CompactInputTypes<wordsize> >(filename);
			case libmaus2::suffixsort::bwtb3m::BwtMergeSortOptions::bwt_merge_input_type_pac:
				return getFileSize<libmaus2::suffixsort::PacInputTypes<wordsize> >(filename);
			case libmaus2::suffixsort::bwtb3m::BwtMergeSortOptions::bwt_merge_input_type_pacterm:
				return getFileSize<libmaus2::suffixsort::PacTermInputTypes<wordsize> >(filename);
			case libmaus2::suffixsort::bwtb3m::BwtMergeSortOptions::bwt_merge_input_type_lz4:
				return getFileSize<libmaus2::suffixsort::Lz4InputTypes<wordsize> >(filename);
			case libmaus2::suffixsort::bwtb3m::BwtMergeSortOptions::bwt_merge_input_type_utf_8:
				return getFileSize<libmaus2::suffixsort::Utf8InputTypes<wordsize> >(filename);
			default:
			{
				libmaus2::exception::LibMausException lme;
				lme.getStream() << "libmaus2::suffixsort::bwtb3m::InputTypesForward::getFileSize: unknown/unsupported input type" << std::endl;
				lme.finish();
				throw lme;
			}
				break;
		}

	}

	static uint64_t getFileSize(std::string const & filename, libmaus2::suffixsort::bwtb3m::BwtMergeSortOptions::bwt_merge_input_type const inputtype, unsigned int wordsize)
	{
		switch ( wordsize )
		{
			case 32:
				return getFileSize<32>(filename, inputtype);
			case 64:
				return getFileSize<64>(filename, inputtype);
			default:
			{
				libmaus2::exception::LibMausException lme;
				lme.getStream() << "[E] libmaus2::suffixsort::bwtb3m::InputTypesForward::getFileSize: unsupported word size " << wordsize << std::endl;
				lme.finish();
				throw lme;
			}
		}
	}

};
#endif
